SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.


; write My First Script in 
^j::
Send, My First Script
return

; write math block inline for restructured text
+!^m::
Send, :math:``
return 

; write math block  for restructured text
!^m::
Send, .. math:: \n \t \frac{ \sum_{t=0}^{N}f(t,k) }{N}
return
; Run comander

!^t::
Run, C:\tools\cmder\cmder.exe 
Return

!^w::
IfWinExist, %Window%
Winclose, %Window%
else
run, \\wsl.localhost\Ubuntu-20.04\home\irdof\%Window%
Return







